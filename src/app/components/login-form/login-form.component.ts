import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { FormsModule, NgForm } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login-form',
  standalone: true,
  imports: [CommonModule, MatInputModule, MatFormFieldModule, MatButtonModule, FormsModule],
  templateUrl: './login-form.component.html',
  styleUrl: './login-form.component.scss',
})
export class LoginFormComponent {
  login = '';
  password = '';

  constructor(
    private readonly authSrv: AuthService,
    private readonly router: Router,
    private toastr: ToastrService,
  ) {}

  async onSubmit(form: NgForm) {
    if (form.valid) {
      try {
        await this.authSrv.login(this.login, this.password);
        this.router.navigate(['/']);
        this.toastr.success('Connexion réussie');
      } catch {
        this.password = '';
        this.toastr.warning('Identifiant ou mot de passe incorrect');
      }
    }
  }
}
