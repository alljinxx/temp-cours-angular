import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule } from '@angular/router';
import { Article } from '../../models/article.model';
import { ArticleService } from '../../services/article.service';
import { AuthService } from '../../services/auth.service';
import { ArticleThumbnailComponent } from '../article/article-thumbnail/article-thumbnail.component';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [CommonModule, ArticleThumbnailComponent, MatButtonModule, RouterModule],
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss',
})
export class HomeComponent implements OnInit {
  articles: Article[] = [];

  constructor(
    private articleSrv: ArticleService,
    readonly authSrv: AuthService,
  ) {}

  async ngOnInit(): Promise<void> {
    // const results = await this.articleSrv.retrieveArticles();
    // this.articles = results.slice(0, 3);

    this.authSrv.currentUser$.subscribe(async () => {
      if (this.authSrv.isLoggedIn) {
        const results = await this.articleSrv.retrieveArticles();
        this.articles = results.slice(0, 3);
      }
    });
  }
}
