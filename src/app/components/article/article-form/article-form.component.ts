import { CommonModule } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Article, CategoryEnum } from '../../../models/article.model';
import { ArticleService } from '../../../services/article.service';

@Component({
  selector: 'app-article-form',
  standalone: true,
  imports: [
    CommonModule,
    MatInputModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatSelectModule,
    MatIconModule,
  ],
  templateUrl: './article-form.component.html',
  styleUrl: './article-form.component.scss',
})
export class ArticleFormComponent implements OnInit {
  formGroup = this.formBuilder.group({
    domain: ['', Validators.required],
    name: ['', Validators.required],
    averageRating: [
      null as number | null,
      [Validators.required, Validators.min(0), Validators.max(5)],
    ],
    category: [CategoryEnum.ROUGE, Validators.required],
    image: ['', Validators.required],
    price: [null as number | null],
    updateAt: [new Date()],
  });

  categoryEnum = CategoryEnum;

  @Input()
  articleId?: string;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly toastr: ToastrService,
    private readonly articleSrv: ArticleService,
    private readonly router: Router,
  ) {}

  async ngOnInit(): Promise<void> {
    if (this.articleId) {
      const article = await this.articleSrv.findArticle(this.articleId);
      this.formGroup.patchValue(article);
    }
  }

  async onSubmit() {
    if (this.formGroup.valid) {
      await this.articleSrv.upsertArticle({
        ...this.formGroup.value,
        objectId: this.articleId,
      } as Article);
      this.toastr.success('Article enregistré avec succès.');
      this.router.navigate(['/articles']);
    } else {
      this.toastr.warning('Il existe des erreurs sur le formulaire.');
    }
  }

  async onDeleteClick() {
    if (
      confirm(
        'Êtes vous sûr de vouloir supprimer cet article, cette action est irréversible ?',
      )
    ) {
      await this.articleSrv.deleteArticle(this.articleId as string);
      this.toastr.success('Article supprimé avec succès.');
      await this.router.navigate(['/articles']);
    }
  }
}
