import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule } from '@angular/router';
import { BarRatingModule } from 'ngx-bar-rating';
import { Article } from '../../../models/article.model';
import { ArticleService } from '../../../services/article.service';
import { ArticleThumbnailComponent } from '../article-thumbnail/article-thumbnail.component';

@Component({
  selector: 'app-articles',
  standalone: true,
  imports: [
    CommonModule,
    MatExpansionModule,
    MatButtonModule,
    MatIconModule,
    BarRatingModule,
    ArticleThumbnailComponent,
    RouterModule,
  ],
  templateUrl: './articles.component.html',
  styleUrl: './articles.component.scss',
})
export class ArticlesComponent implements OnInit {
  /**
   * Liste des articles
   */
  articles: Article[] = [];

  /**
   * Affichage en mode tuiles, sinon cases
   */
  tilesDisplay = false;

  /**
   * Nom du vin dont la tuile ouverte
   */
  openedTileName?: string;

  constructor(
    private httpClient: HttpClient,
    private readonly articleSrv: ArticleService,
  ) {}

  async ngOnInit(): Promise<void> {
    this.articles = await this.articleSrv.retrieveArticles();
  }
}
