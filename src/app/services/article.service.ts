import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, lastValueFrom, map, take } from 'rxjs';
import { API_URL } from '../../misc/globals';
import { Article } from '../models/article.model';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root',
})
export class ArticleService {
  constructor(
    private readonly httpClient: HttpClient,
    private readonly authSrv: AuthService,
  ) {}

  retrieveArticles() {
    const request$ = this.httpClient
      .get<{
        results: Article[];
      }>(`${API_URL}/classes/Article`)
      .pipe(
        take(1),
        map((elem) => elem.results),
      );

    return lastValueFrom(request$);
  }

  async upsertArticle(article: Article) {
    let request$: Observable<unknown>;

    if (article.objectId) {
      request$ = this.httpClient.put(
        `${API_URL}/classes/Article/${article.objectId}`,
        article,
      );
    } else {
      request$ = this.httpClient.post(`${API_URL}/classes/Article`, article);
    }

    request$.pipe(take(1));
    return lastValueFrom(request$);
  }

  async findArticle(articleId: string) {
    const request$ = this.httpClient
      .get<Article>(`${API_URL}/classes/Article/${articleId}`)
      .pipe(take(1));

    return lastValueFrom(request$);
  }

  async deleteArticle(articleId: string) {
    const request$ = this.httpClient
      .delete(`${API_URL}/classes/Article/${articleId}`)
      .pipe(take(1));

    return lastValueFrom(request$);
  }
}
