import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, lastValueFrom, take } from 'rxjs';
import { API_URL } from '../../misc/globals';
import { UserSession } from '../models/user-session.model';

const LOCAL_STORAGE_AUTH_KEY = 'currentUser';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  currentUser$ = new BehaviorSubject<UserSession | undefined>(undefined);

  get isLoggedIn(): boolean {
    // if (this.currentUser) {
    //   return true;
    // } else {
    //   return false;
    // }

    return this.currentUser$.value !== undefined;
  }

  constructor(
    private readonly httpClient: HttpClient,
    private router: Router,
  ) {
    this.restoreSession();
  }

  restoreSession() {
    const session = localStorage.getItem('currentUser');
    if (session) {
      this.currentUser$.next(JSON.parse(session));
    }
  }

  async login(login: string, password: string): Promise<void> {
    const request$ = this.httpClient
      .post<UserSession>(`${API_URL}/login`, { username: login, password: password })
      .pipe(take(1));

    this.currentUser$.next(await lastValueFrom(request$));
    localStorage.setItem(LOCAL_STORAGE_AUTH_KEY, JSON.stringify(this.currentUser$.value));
  }

  async logout(): Promise<void> {
    this.httpClient.post(`${API_URL}/logout`, undefined).pipe(take(1));

    this.currentUser$.next(undefined);
    localStorage.removeItem(LOCAL_STORAGE_AUTH_KEY);

    this.router.navigate(['/']);
  }
}
