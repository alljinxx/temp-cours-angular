export enum CategoryEnum {
  ROUGE = 'rouge',
  ROSE = 'rose',
  BLANC = 'blanc',
}

export interface Article {
  /**
   * Id de l'objet
   */
  objectId?: string;

  /**
   * Domaine de fabrication
   */
  domain: string;

  /**
   * Nom du vin
   */
  name: string;

  /**
   * Note moyenne
   */
  averageRating: number;

  /**
   * Catégorie : blanc, rosé ou blanc
   */
  category: CategoryEnum;

  /**
   * Photo de la bouteille
   */
  image: string;

  /**
   * Prix
   */
  price?: number;

  /**
   * Date de dernière modification
   */
  updatedAt: Date;
}
