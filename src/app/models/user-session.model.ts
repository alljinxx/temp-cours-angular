export interface UserSession {
  username: string;
  objectId: string;
  sessionToken: string;
}
