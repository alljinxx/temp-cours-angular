import { Routes } from '@angular/router';
import { authGuard } from '../misc/auth.guard';

export const routes: Routes = [
  {
    path: 'accueil',
    loadComponent: () => import('./components/home/home.component').then((m) => m.HomeComponent),
  },
  {
    path: 'articles',
    loadComponent: () =>
      import('./components/article/articles/articles.component').then((m) => m.ArticlesComponent),
    canActivate: [authGuard],
  },
  {
    path: 'article/creation',
    loadComponent: () =>
      import('./components/article/article-form/article-form.component').then(
        (m) => m.ArticleFormComponent,
      ),
    canActivate: [authGuard],
  },
  {
    path: 'article/modification/:articleId',
    loadComponent: () =>
      import('./components/article/article-form/article-form.component').then(
        (m) => m.ArticleFormComponent,
      ),
    canActivate: [authGuard],
  },
  {
    path: 'identification',
    loadComponent: () =>
      import('./components/login-form/login-form.component').then((m) => m.LoginFormComponent),
  },
  { path: '**', redirectTo: 'accueil' },
];
