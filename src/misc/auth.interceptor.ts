import { HttpInterceptorFn } from '@angular/common/http';
import { inject } from '@angular/core';
import { AuthService } from '../app/services/auth.service';

export const authInterceptor: HttpInterceptorFn = (req, next) => {
  const authSrv = inject(AuthService);

  const headers: Record<string, string> = { 'X-Parse-Application-Id': 'cours_angular' };

  if (authSrv.isLoggedIn) {
    headers['X-Parse-Session-Token'] = authSrv.currentUser$.value?.sessionToken as string;
  }

  const authReq = req.clone({ setHeaders: headers });

  return next(authReq);
};
