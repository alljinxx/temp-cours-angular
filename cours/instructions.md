# Instructions du TP

## Création du projet

Installation d'Angular CLI :
`npm install -g @angular/cli`

Création de l'application Angular :
`ng new mon-application-angular`

Configurer Prettier

Ajouter EsLint : `ng add @angular-eslint/schematics`

> Conseils
>
> 1. Ne pas installer Angular globalement (conflits de version)
> 2. Laisser la mise à jour de Typescript à ng update

## Création du composant Header

Création du composant :
`ng g c components/header`

Ajout du header dans AppComponent

Ajouter un bouton "Se connecter/Se déconnecter"

Lancer l'application : `ng serve`

## Ajout d'Angular Materials

Installer Angular Materials : `npx ng add @angular/material`

Personnaliser le Header avec une toolBar

## Création des composants "Home", "Articles, "Article" et routing

Création des composants :
`ng g c components/home`
`ng g c components/article/articles`
`ng g c components/article/article`
`ng g c components/article/article-form`

Création des routes vers ces nouveaux composants et ajout d'un lien pour accéder à la liste des articles dans le header.

## Implémentation de la liste des articles

Création des "fakes" données : https://alcyoneus.feralhosting.com/jillnax/app.data.ts

Modifier le composant "Articles" pour afficher les articles

## Ajout d'un affichage en tuiles pour la liste des articles

1. Utilisation du mat-expansion-panel et du mat-accordion
2. Affichae du nom du vin (de la couleur du vin) et du domaine dans l'en-tête, l'image, le prix et la note dans le collapse
3. Ajout d'un bouton pour switcher d'une disposition à l'autre
4. Améliorer l'affichage de la note en utilisant `ngx-bar-rating`

## Mutualisation : déport de l'affichage d'un article en vignette dans un composant dédié

1. Création d'un composant `article-thumbnail`
2. Création d'un service pour charger les données des articles
3. Utilisation dans la page d'accueil pour afficher les 3 premiers vins de la liste
4. Ajouter un bouton "voir plus"
